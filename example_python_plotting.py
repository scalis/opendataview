'''
Created on Dec 04, 2015

@author: stijn

PyPlot Info
http://matplotlib.org/api/pyplot_api.html
http://matplotlib.org/faq/howto_faq.html



'''
import lib.opendata

import os

import matplotlib.pyplot as plt
plt.rcdefaults()
import numpy as np

import logging
logging.basicConfig(level = logging.DEBUG)

def get_poop_data():
    link_hondenpoep = 'http://datatank.stad.gent/4/milieuennatuur/tellijsthondenpoep2010.json'
    MONTH = 'juni_2010'
    #MONTH = 'december_10'
    opendata = lib.opendata.OpenData(link_hondenpoep)
    poop_data = {}
    
    for data in opendata.read():
        location_str = data['locatie']
        poop_amount = 0
        try:
            poop_amount = data[MONTH]
        except:
            pass
        
        if not poop_amount == 0 and 'totaal' in location_str and not 'Algemeen' in location_str:
            if not poop_amount == 0:
                poop_data[location_str.replace('totaal ','')] = poop_amount 
    
    return poop_data  

def get_birth_data():
    link_birth_data = 'http://datatank.stad.gent/4/bevolking/geboortes.json'
    opendata = lib.opendata.OpenData(link_birth_data).read()
    
    birth_data = {}
    
    
    columns = []
    for item in opendata:
        for k,v in item.iteritems():
            if k == 'wijk':
                columns.append(v)
    

    print columns
    arr_headers = None
    #for data in :
        
        

def create_plotting_poopdata(poop_data = None,plot_type = 'barchart'):
    '''
    @param plot_type: barchart,
    ''' 
    logging.debug('create plotting')
    
    streets = poop_data.keys()
    kg = poop_data.values()    
    plt.title('Street Data')
    
    if plot_type == 'barchart':
        y_pos = np.arange(len(streets))
        amount = np.array(kg)
    
        plt.barh(y_pos, amount, align='center', alpha=0.4)
        plt.yticks(y_pos, streets)
        plt.xlabel('KG')
        
        plt.gcf().subplots_adjust(left=0.3)

    plt.show()
    
def create_plotting_birthdata(birth_data = None,plot_type = 'barchart'):
    '''
    http://matplotlib.org/examples/pylab_examples/table_demo.html
    ''' 
    logging.debug('create plotting')
    
    raw_input('wait')
    #for k,v,
    
    arr_months = birth_data[0].keys()
    
    columns = np.array(arr_months)
    print columns
    
    streets = birth_data.keys()
    kg = birth_data.values()    
    plt.title('Births Data')
    
    if plot_type == 'barchart':
        y_pos = np.arange(len(streets))
        amount = np.array(kg)
    
        plt.barh(y_pos, amount, align='center', alpha=0.4)
        plt.yticks(y_pos, streets)
        plt.xlabel('KG')
        
        plt.gcf().subplots_adjust(left=0.3)

    plt.show()
    
def view_poop_data():
    poop_data = get_poop_data()         
    create_plotting_poopdata(poop_data)
    
def view_birth_data():
    birth_data = get_birth_data()
    create_plotting_birthdata(birth_data)
    
def main():
    view_poop_data()
    #view_birth_data()
    

if __name__ == '__main__':
    #main()
    main()
    pass