'''
Created on Oct 28, 2015

@author: stijn
'''
import lib.opendata
import lib.fusion_api as fusion_api

import os
import logging
logging.basicConfig(level = logging.DEBUG)

def get_poop_data():
    link_hondenpoep = 'http://datatank.stad.gent/4/milieuennatuur/tellijsthondenpoep2010.json'
    MONTH = 'juni_2010'
    #MONTH = 'december_10'
    opendata = lib.opendata.OpenData(link_hondenpoep)
    poop_data = {}
    
    for data in opendata.read():
        location_str = data['locatie']
        poop_amount = 0
        try:
            poop_amount = data[MONTH]
        except:
            pass
        
        if not poop_amount == 0 and 'totaal' in location_str and not 'Algemeen' in location_str:
            if not poop_amount == 0:
                poop_data[location_str.replace('totaal ','')] = poop_amount 
    
    return poop_data   

def create_chart_fusion(poop_data = None):
    offset = 100
    
    poop_particle_multipy = 1
    poop_overload = 100
    
    comp_obj = fusion_api.Comp()
    current_frame = 0
    
    comp_obj.set_current_frame(current_frame)
    comp_obj.set_global_start(current_frame)
    comp_obj.set_render_start(current_frame)
    
    pEmit = comp_obj.get_tool('pEmit')
    pEmit.AddModifier("Number","BezierSpline")
    
    text_output = comp_obj.get_tool('text_output')
    
    #AddModifer = animate a property of a tool
    text_output.AddModifier('StyledText','BezierSpline')
    text_output.AddModifier('End','BezierSpline')
    
    cc_alarm = comp_obj.get_tool('cc_alarm')
    cc_alarm.AddModifier('Blend','BezierSpline')
    
    #Set value on first frame to zero
    cc_alarm.Blend[current_frame] = 0

    if poop_data != None:
        for location in poop_data:
            poop_amount = poop_data[location]
            msg = '{}: {} KG'.format(str(location).upper(),str(poop_amount))
            
            text_output.StyledText[current_frame] = msg
            
            text_output.End[current_frame] = 0
            text_output.End[current_frame + int(offset*0.2)] = 1
            text_output.End[current_frame + offset-2] = 1
            text_output.End[current_frame + offset-1] = 0
            
            pEmit.Number[current_frame - 1] = 0
            pEmit.Number[current_frame] = int(poop_amount) * poop_particle_multipy
            pEmit.Number[current_frame + 1] = 0
            
            print '\n'+msg
            
            if poop_amount > poop_overload:
                print 'That is a lot of crap in {}!'.format(location)
                cc_alarm.Blend[current_frame] = 0
                cc_alarm.Blend[current_frame + int(offset*0.2)] = 1
                cc_alarm.Blend[current_frame + offset - int(offset*0.2)] = 1
                cc_alarm.Blend[current_frame + offset-1] = 0
                
            
            current_frame += offset
    
    comp_obj.set_global_end(current_frame + offset)
    comp_obj.set_render_end(current_frame + offset)
    
    render_path = os.path.abspath('C:/Temp/render/')
    if not os.path.exists(render_path):
        os.makedirs(render_path)
    
    
def main():
    poop_data = get_poop_data()     
    print poop_data      
    raw_input('is data correct?')
    create_chart_fusion(poop_data)
    

if __name__ == '__main__':
    #main()
    main()
    pass