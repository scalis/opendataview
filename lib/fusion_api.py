import PeyeonScript as eyeon

import json
import logging
logging.basicConfig(level = logging.DEBUG)

class Fusion():
    def __init__(self):
        self._fusion = eyeon.scriptapp('Fusion')
        
    def get_fusion(self):
        return self._fusion
    
    def __repr__(self):
        self.get_fusion()
        
    def get_current_comp(self):
        return self._fusion.GetCurrentComp()
    
class Comp():
    def __init__(self,current_comp = True):
        if current_comp is True:
            self.comp = Fusion().get_current_comp()
        else:
            pass
    
    def __repr__(self):
        return self.comp
        
    def get_name(self):
        return self.comp.GetAttrs('COMPS_Name')
        
    
    #    FRAME RANGE SETTINGS
    def set_current_frame(self,int):
        self.comp.SetAttrs({'COMPN_CurrentFrame': int})
        return True
    
    def get_global_start(self):
        return self.comp.GetAttrs('COMPN_GlobalStart')
    
    def set_global_start(self,int):
        self.comp.SetAttrs({'COMPN_GlobalStart': int})
        return True
    
    def get_global_end(self):
        return self.comp.GetAttrs('COMPN_GlobalEnd')
    
    def set_global_end(self,int):
        self.comp.SetAttrs({'COMPN_GlobalEnd': int})
        return True
    
    def get_render_start(self):
        return self.comp.GetAttrs('COMPN_RenderStart')
    
    def set_render_start(self,int):
        self.comp.SetAttrs({'COMPN_RenderStart': int})
        return True
    
    def get_render_end(self):
        return self.comp.GetAttrs('COMPN_RenderEnd')
    
    def set_render_end(self,int):
        self.comp.SetAttrs({'COMPN_RenderEnd': int})
        return True
    
    #    COMP INFO
    def show_comp_attributes(self,json_dump_loc = None):
        self.comp.Lock()
        attrs = self.comp.GetAttrs()
        
        try:
            return show_value_list(attrs,json_dump_loc)
        except:
            logging.error('issue on show_comp_attributes')
            
        self.comp.Unlock()
            
    def show_comp_prefs(self,json_dump_loc = None):
        self.comp.Lock()
        prefs = self.comp.GetPrefs()
        try:
            return show_value_list(prefs,json_dump_loc)
        except:
            logging.error('issue on show_comp_prefs.')
        self.comp.Unlock()
   
#    TOOLS
    def get_tools(self):
        tool_list = self.comp.GetToolList()
        tool_obj_list = []
        for tool_index in tool_list:
            c_tool = tool_list[tool_index]
            tool_obj_list.append(c_tool)
            
        return tool_obj_list
    
    def get_tool(self,toolname):
        for tool in self.get_tools():
            if tool.GetAttrs('TOOLS_Name') == toolname:
                return tool

class Tool():
    def __init__(self,_tool = None):
        self._tool = _tool
        
        if self._tool != None:
            self._tool
            
    def __repr__(self):
        return self._tool
            
    def get_name(self):
        return self._tool.GetAttrs('TOOLS_Name')
        
    def show_tool_attributes(self,json_dump_loc = None):
        #self.comp.Lock()
        attrs = self._tool.GetAttrs()
        
        try:
            return show_value_list(attrs,json_dump_loc)
        except:
            logging.error('issue on show_tool_attributes')
            
        self.comp.Unlock()

#    Generic Functions
def show_value_list(value_list,json_dump_loc = None):
    for value_title in value_list:
            value = value_list[value_title]
            debug_line = '%s\t%s'%(value_title,value)
            logging.info(debug_line)

    if json_dump_loc != None:
        f = open(json_dump_loc,'w+')
        json_string = json.dumps(obj = value_list, ensure_ascii = True, indent = 4)
        f.write(json_string)
        f.close()
        
if __name__ == '__main__':
    fusion = Fusion()
    print fusion.get_current_comp()
    
    pass