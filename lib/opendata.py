import urllib,json
import unicodedata
import numbers

class OpenData():
    def __init__(self,filename):
        self.filename = filename 
        
    def encode_data(self):
        '''
        encode unicode to string
        type_cast the values to integer, float, None (if empty)
        '''
        encoded_data = []
        for data_item in self.data:
            encoded_data_item = {}
            for k,v in data_item.iteritems():
                if type(k) is unicode:
                    k = unicodedata.normalize('NFKD', k).encode('ascii','ignore')
                if type(v) is unicode:
                    v = unicodedata.normalize('NFKD', v).encode('ascii','ignore')
                    
                    type_cast_v = False
                    
                    #    Check if number
                    try:
                        type_cast_v = int(v)
                    except:
                        pass
                    
                    #    Check if float
                    if ',' in v:
                        v_split = v.split(',')
                        print v_split
                        if len(v_split) == 2:
                            try:
                                v_1 = int(v_split[0])
                                v_2 = int(v_split[1])
                                corrected_float = v_split[0]+'.'+v_split[1]
                                type_cast_v = float(corrected_float)
                            except:
                                pass
                    
                    #    Check if empty
                    if len(v) == 0:
                        type_cast_v = None
                        
                    
                    #    If type_cast succeeded, overwrite value
                    if type_cast_v != False:
                        v = type_cast_v
                    
                    encoded_data_item[k] = v    
                    
            encoded_data.append(encoded_data_item)
        self.data = encoded_data
        
    def read_url(self):
        response = urllib.urlopen(self.filename)
        return json.loads(response.read())
    
    def read_file(self):
        f_handler = open(self.filename,mode="r")
        f_data = f_handler.read()
        f_data.encode('utf-8')
        f_handler.close()
        return json.loads(f_data)
    
    def read(self):
        self.data = self.read_url() if 'http:' in self.filename else self.read_file(self.filename)
        self.encode_data()
        return self.data
        
        
if __name__ == '__main__':
    link = 'http://datatank.stad.gent/4/mobiliteit/bezettingparkingsrealtime.json'
    link_hondenpoep = 'http://datatank.stad.gent/4/milieuennatuur/tellijsthondenpoep2010.json'
    link_vogels = 'http://datatank.stad.gent/4/milieuennatuur/vogeltellingbourgoyenwaarneming.json'
    link_births = 'http://datatank.stad.gent/4/bevolking/geboortes.json'
    opendata = OpenData(link_births)
    for data in opendata.read():
        for k,v in data.iteritems():
            print k + ': ' + str(v)
        print '\n'